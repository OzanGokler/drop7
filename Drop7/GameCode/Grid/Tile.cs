﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drop7.GameCode.Grid
{
    class Tile
    {
        public int iValue = -1;
        public bool bMoving = false;
        public bool bFlaggedToDestroy = false;

        public void SetTile ( int iNewValue )
        {
            iValue = iNewValue;
            bMoving = true;
        }

        public void EmptyMe()
        {
            iValue = -1;
            bMoving = false;
            bFlaggedToDestroy = false;
        }
    }
}
