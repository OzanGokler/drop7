﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drop7.GameCode.Grid
{
    class Grid
    {
        public int iRowCount;
        public int iColumnCount;
        public Tile[][] Tiles;

        public Grid(int iRow, int iColumn)
        {
            iRowCount = iRow;
            iColumnCount = iColumn;
            SetUpGrid();
        }

        public Tile GetTile(int iRow, int iColumn)
        {
            Tile tRet = Tiles[iRow][iColumn];
            return tRet;
        }

        public void SetUpGrid()
        {
            Tiles = new Tile[iRowCount][];
            for(int iRow=0; iRow < iRowCount; iRow++)
            {
                Tile[] CurrentRow = new Tile[iColumnCount];
                for(int iColumn = 0 ; iColumn < iColumnCount; iColumn++)
                {
                    Tile CurrentTile = new Tile();
                    CurrentRow[iColumn] = CurrentTile;
                }
                Tiles[iRow] = CurrentRow;
            }
        }
    }
}
