﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drop7
{
    class GameStatic
    {
        public static MetaGame TheGame;
        public static Texture2D imgTileBackgraund;
        public static Texture2D[] imgTiles;
        public static Texture2D imgGameOver;
        public static SpriteBatch spriteBatch;
        public static bool bGameOver = false;

        public static void SetUpGame( int iRow, int iColumn)
        {
            TheGame = new MetaGame(iRow, iColumn);
        }

        public static void Draw()
        {
            TheGame.Draw();
        }
        
        public static void Update(GameTime gameTime)
        {
            TheGame.Update(gameTime);
        }
    }
}
