﻿using Drop7.GameCode.Grid;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drop7
{
    class MetaGame
    {
        Grid gGrid;
        int gameScore = 0;
        public int iDrawStartX = 20;
        public int iDrawStartY = 150;
        int iCurrentNumber = 3;
        int iCurrentPosition = 0;
        float fDropStart = 0;
        float fDropTime = 100;
        int iMovingOffset;
        Random rndRandomGenerator;
        GameState CurrentState = GameState.SelectingColumn;
        enum GameState
        {
            SelectingColumn,
            FallingBlock,
            Evaluating
        }
        public MetaGame( int iRow, int iColumn)
        {
            gGrid = new Grid(iRow, iColumn);
            rndRandomGenerator = new Random();
            iCurrentNumber = rndRandomGenerator.Next(0, 7);
        }
        public void Draw()
        {
            if(CurrentState == GameState.SelectingColumn)
            {
                Rectangle rSelector = new Rectangle(iDrawStartX + (iCurrentPosition * 100), iDrawStartY - 100, 100, 100);
                GameStatic.spriteBatch.Draw(GameStatic.imgTiles[iCurrentNumber], rSelector, Color.White);
            }
            for (int iRow = 0; iRow < gGrid.iRowCount; iRow++)
            {
                for (int iColumn = 0; iColumn < gGrid.iColumnCount; iColumn++)
                {
                    Tile tCurrent = gGrid.GetTile(iRow, iColumn);
                    Rectangle rDestination = new Rectangle(iDrawStartX + (iColumn * 100), iDrawStartY + (iRow * 100), 100, 100);
                    if(tCurrent.iValue > -1)
                    {
                        if(tCurrent.bMoving)
                        {
                            Rectangle rMoving = new Rectangle(iDrawStartX + (iColumn * 100), iDrawStartY + (iRow * 100) + iMovingOffset - 100, 100, 100);
                            GameStatic.spriteBatch.Draw(GameStatic.imgTiles[tCurrent.iValue], rMoving, Color.White);
                        }
                        else
                        {
                            GameStatic.spriteBatch.Draw(GameStatic.imgTiles[tCurrent.iValue], rDestination, Color.White);
                        }
                    }
                    GameStatic.spriteBatch.Draw(GameStatic.imgTileBackgraund, rDestination, Color.White);
                }
            }
        }
        public void Update(GameTime gametime)
        {
            if (CurrentState == GameState.SelectingColumn)
            {
                SelectingColumn(gametime);
            }
            else if(CurrentState == GameState.FallingBlock)
            {
                FallingBlock(gametime);
            }
            else if(CurrentState == GameState.Evaluating)
            {
                Evaluating(gametime);
            }
        }

        KeyboardState kOldState;

        public void SelectingColumn( GameTime gameTime)
        {
            if(Keyboard.GetState().IsKeyUp(Keys.Right) && kOldState.IsKeyDown(Keys.Right))
            {
                if(iCurrentPosition < 6)
                {
                    iCurrentPosition++;
                }
            }
            if(Keyboard.GetState().IsKeyUp(Keys.Left) && kOldState.IsKeyDown(Keys.Left))
            {
                if(iCurrentPosition > 0)
                {
                    iCurrentPosition--;
                }
            }
            if (Keyboard.GetState().IsKeyUp(Keys.Space) && kOldState.IsKeyDown(Keys.Space))
            {
                EvaluateGameOver();
                gGrid.Tiles[0][iCurrentPosition].SetTile(iCurrentNumber);
                StartFallProcess(gameTime);
            }

            kOldState = Keyboard.GetState();
        }
        void FallingBlock(GameTime gameTime)
        {
            float fTimePassed = (float)gameTime.TotalGameTime.TotalMilliseconds - fDropStart;
            iMovingOffset = (int)MathHelper.Lerp(0, 100, fTimePassed / fDropTime);
            if (fTimePassed >= fDropTime)
            {
                StopMoving();
                fDropStart = (float)gameTime.TotalGameTime.TotalMilliseconds;
                bool bDropping = DropBlocks();
                if(!bDropping)
                    CurrentState = GameState.Evaluating;
            }
        }
        bool DropBlocks()
        {
            bool bRet = false;
            for(int iRowCounter = gGrid.iRowCount -2 ; iRowCounter > -1; iRowCounter-- )
            {
                for(int iColumnCounter = 0; iColumnCounter < gGrid.iColumnCount; iColumnCounter++)
                {
                    Tile CurrentTile = gGrid.GetTile(iRowCounter, iColumnCounter);
                    Tile TileBelow = gGrid.GetTile(iRowCounter + 1, iColumnCounter);
                    if(CurrentTile.iValue > -1 && TileBelow.iValue == -1)
                    {
                        TileBelow.SetTile(CurrentTile.iValue);
                        CurrentTile.EmptyMe();
                        bRet = true;
                    }
                }
            }
            return bRet;
        }

        void DestroyFlagged(GameTime gameTime)
        {
            for( int iRowCounter = 0; iRowCounter < gGrid.iRowCount; iRowCounter++)
            {
                for(int iColumnCounter = 0; iColumnCounter < gGrid.iColumnCount; iColumnCounter++)
                {
                    Tile CurrentTile = gGrid.GetTile(iRowCounter, iColumnCounter);
                    if(CurrentTile.bFlaggedToDestroy)
                    {
                        CurrentTile.EmptyMe();
                    }
                }
            }
            StartFallProcess(gameTime);
        }

         void StartFallProcess(GameTime gameTime)
        {
            fDropStart = (float)gameTime.TotalGameTime.TotalMilliseconds;
            CurrentState = GameState.FallingBlock;
        }
        void GoBackToSElecting()
         {
             iCurrentNumber = rndRandomGenerator.Next(0, 7);
             CurrentState = GameState.SelectingColumn;
         }
        void StopMoving()
        {
            iMovingOffset = 0;
            for(int iRowCounter = 0; iRowCounter < gGrid.iRowCount; iRowCounter++)
            {
                for(int iColumnCounter = 0; iColumnCounter < gGrid.iColumnCount; iColumnCounter++)
                {
                    Tile CurrentTile = gGrid.GetTile(iRowCounter, iColumnCounter);
                    CurrentTile.bMoving = false;
                }
            }
        }
        void EvaluateGameOver()
        {
            if(gGrid.Tiles[0][iCurrentPosition].iValue > -1)
            {
                Console.WriteLine("GAME OVER");
                GameStatic.bGameOver = true;
            }
        }

        void Evaluating(GameTime gameTime)
        {
            bool bAnyFlagged = false;
            for(int iRowCounter = 0; iRowCounter < gGrid.iRowCount; iRowCounter++)
            {
                int iCurrentCount = 0;
                for (int iColumnCounter = 0; iColumnCounter < gGrid.iColumnCount; iColumnCounter++ )
                {
                    Tile CurrenTile = gGrid.GetTile(iRowCounter, iColumnCounter);
                    if(CurrenTile.iValue > -1)
                    {
                        iCurrentCount++;
                    }
                    if(CurrenTile.iValue == -1 || iColumnCounter == gGrid.iColumnCount - 1)
                    {
                        if(iCurrentCount > 0)
                        {
                            for( int iCollectedItems = 0; iCollectedItems < iCurrentCount; iCollectedItems++)
                            {
                                int iColumnToUse = iColumnCounter - (iCollectedItems + 1);
                                if(iColumnCounter == gGrid.iColumnCount -1)
                                {
                                    iColumnToUse = iColumnCounter - (iCollectedItems);
                                }
                                Tile EvaluatedTile = gGrid.GetTile(iRowCounter, iColumnToUse);
                                if(EvaluatedTile.iValue + 1 == iCurrentCount )
                                {
                                    EvaluatedTile.bFlaggedToDestroy = true;
                                    bAnyFlagged = true;
                                }
                            }
                        }
                        iCurrentCount = 0;
                    }
                }

            }
            if (bAnyFlagged)
            {
                DestroyFlagged(gameTime);
                Console.Clear();
                gameScore = gameScore + 7;
                Console.WriteLine("Point : " + gameScore);
            }
            else
                GoBackToSElecting();
        }

    }
}
