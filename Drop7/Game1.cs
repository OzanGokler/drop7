using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Drop7
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            graphics.IsFullScreen = false;
            graphics.PreferredBackBufferHeight = 900;
            graphics.PreferredBackBufferWidth = 800;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            GameStatic.SetUpGame(7, 7);
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            GameStatic.spriteBatch = new SpriteBatch(GraphicsDevice);
            GameStatic.imgTileBackgraund = Content.Load<Texture2D>("Sprites/bgtile");
            GameStatic.imgGameOver = Content.Load<Texture2D>("Sprites/gameover");

            GameStatic.imgTiles = new Texture2D[7];
            for(int i=0; i<7; i++)
            {
                string sNumber = (i + 1).ToString();
                GameStatic.imgTiles[i] = Content.Load<Texture2D>("Sprites/Block/tile" + sNumber);
            }
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if(!GameStatic.bGameOver)
                GameStatic.Update(gameTime);
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            GameStatic.spriteBatch.Begin();

            if (!GameStatic.bGameOver)
            {
                GameStatic.Draw();
            }
            else
            {
                Rectangle rGameOver = new Rectangle(100, 300, 640, 300);
                GameStatic.spriteBatch.Draw(GameStatic.imgGameOver, rGameOver, Color.White);
            }
            GameStatic.spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
